import java.util.Scanner;

public class StaircaseTest {

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if(n<=100){
            for(int a=0;a<n;a++){

                for(int b=n;b>a;b--){
                    System.out.print("  ");
                }

                for(int c=0;c<=a;c++){
                    System.out.print("# ");

                }

                System.out.println("");
            }
        }else{
            System.out.println("n is out of range!");
        }
    }
}
